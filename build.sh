#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build SP
#
pushd source || exit 1
cd SP
ARCH=x86_64 BUILD_CLIENT=1 BUILD_SERVER=0 BASEGAME=Main  make
popd || exit 1

mkdir tmp || exit 1
mkdir tmp/Main || exit 1

cp -rfv source/SP/build/release-linux-x86_64/*.so tmp/
cp -rfv source/SP/build/release-linux-x86_64/iowolfsp.x86_64 tmp/
cp -rfv source/SP/build/release-linux-x86_64/Main/*.so tmp/Main/

# build MP
#
pushd source || exit 1
cd MP
ARCH=x86_64 BUILD_CLIENT=1 BUILD_SERVER=0 BASEGAME=Main make
popd || exit 1

cp -rfv source/MP/build/release-linux-x86_64/*.so tmp/
cp -rfv source/MP/build/release-linux-x86_64/iowolfmp.x86_64 tmp/
cp -rfv source/MP/build/release-linux-x86_64/Main/*.so tmp/Main/

find tmp/

cp -rfv tmp/* "9010/dist/"

find "9010"
